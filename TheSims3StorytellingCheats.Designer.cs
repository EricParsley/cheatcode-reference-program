﻿namespace Cheatcode_Reference_Program
{
    partial class TheSims3StorytellingCheats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TheSims3StorytellingCheats));
            this.TheSims3StorytellingCheatCloseButton = new System.Windows.Forms.Button();
            this.TheSims3StorytellingCheatsRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // TheSims3StorytellingCheatCloseButton
            // 
            this.TheSims3StorytellingCheatCloseButton.Location = new System.Drawing.Point(515, 78);
            this.TheSims3StorytellingCheatCloseButton.Name = "TheSims3StorytellingCheatCloseButton";
            this.TheSims3StorytellingCheatCloseButton.Size = new System.Drawing.Size(75, 23);
            this.TheSims3StorytellingCheatCloseButton.TabIndex = 0;
            this.TheSims3StorytellingCheatCloseButton.Text = "Close";
            this.TheSims3StorytellingCheatCloseButton.UseVisualStyleBackColor = true;
            this.TheSims3StorytellingCheatCloseButton.Click += new System.EventHandler(this.TheSims3StorytellingCheatCloseButton_Click);
            // 
            // TheSims3StorytellingCheatsRichTextBox
            // 
            this.TheSims3StorytellingCheatsRichTextBox.Location = new System.Drawing.Point(12, 12);
            this.TheSims3StorytellingCheatsRichTextBox.Name = "TheSims3StorytellingCheatsRichTextBox";
            this.TheSims3StorytellingCheatsRichTextBox.ReadOnly = true;
            this.TheSims3StorytellingCheatsRichTextBox.Size = new System.Drawing.Size(579, 60);
            this.TheSims3StorytellingCheatsRichTextBox.TabIndex = 1;
            this.TheSims3StorytellingCheatsRichTextBox.Text = resources.GetString("TheSims3StorytellingCheatsRichTextBox.Text");
            // 
            // TheSims3StorytellingCheats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 106);
            this.Controls.Add(this.TheSims3StorytellingCheatsRichTextBox);
            this.Controls.Add(this.TheSims3StorytellingCheatCloseButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TheSims3StorytellingCheats";
            this.Text = "The Sims 3 Storytelling Cheats";
            this.Load += new System.EventHandler(this.TheSims3StorytellingCheats_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button TheSims3StorytellingCheatCloseButton;
        private System.Windows.Forms.RichTextBox TheSims3StorytellingCheatsRichTextBox;
    }
}