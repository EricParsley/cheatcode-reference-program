﻿namespace Cheatcode_Reference_Program
{
    partial class TheSims3TestingCheats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TheSims3TestingCheats));
            this.TheSims3TestingCheatsCloseButton = new System.Windows.Forms.Button();
            this.TestingCheatsRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // TheSims3TestingCheatsCloseButton
            // 
            this.TheSims3TestingCheatsCloseButton.Location = new System.Drawing.Point(713, 415);
            this.TheSims3TestingCheatsCloseButton.Name = "TheSims3TestingCheatsCloseButton";
            this.TheSims3TestingCheatsCloseButton.Size = new System.Drawing.Size(75, 23);
            this.TheSims3TestingCheatsCloseButton.TabIndex = 0;
            this.TheSims3TestingCheatsCloseButton.Text = "Close";
            this.TheSims3TestingCheatsCloseButton.UseVisualStyleBackColor = true;
            this.TheSims3TestingCheatsCloseButton.Click += new System.EventHandler(this.TheSims3TestingCheatsCloseButton_Click);
            // 
            // TestingCheatsRichTextBox
            // 
            this.TestingCheatsRichTextBox.Location = new System.Drawing.Point(12, 12);
            this.TestingCheatsRichTextBox.Name = "TestingCheatsRichTextBox";
            this.TestingCheatsRichTextBox.ReadOnly = true;
            this.TestingCheatsRichTextBox.Size = new System.Drawing.Size(776, 397);
            this.TestingCheatsRichTextBox.TabIndex = 1;
            this.TestingCheatsRichTextBox.Text = resources.GetString("TestingCheatsRichTextBox.Text");
            // 
            // TheSims3TestingCheats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TestingCheatsRichTextBox);
            this.Controls.Add(this.TheSims3TestingCheatsCloseButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TheSims3TestingCheats";
            this.Text = "The Sims 3 Testing Cheats";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button TheSims3TestingCheatsCloseButton;
        private System.Windows.Forms.RichTextBox TestingCheatsRichTextBox;
    }
}