﻿namespace Cheatcode_Reference_Program
{
    partial class TheSims3Cheats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TheSims3Cheats));
            this.TheSims3CheatsCloseButton = new System.Windows.Forms.Button();
            this.MiscCheatsButton = new System.Windows.Forms.Button();
            this.StorytellingCheatsButton = new System.Windows.Forms.Button();
            this.BuildCheatsButton = new System.Windows.Forms.Button();
            this.GameplayCheatsButton = new System.Windows.Forms.Button();
            this.AdvancedCheatsButton = new System.Windows.Forms.Button();
            this.TestingCheatsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TheSims3CheatsCloseButton
            // 
            this.TheSims3CheatsCloseButton.Location = new System.Drawing.Point(12, 186);
            this.TheSims3CheatsCloseButton.Name = "TheSims3CheatsCloseButton";
            this.TheSims3CheatsCloseButton.Size = new System.Drawing.Size(200, 23);
            this.TheSims3CheatsCloseButton.TabIndex = 0;
            this.TheSims3CheatsCloseButton.Text = "Close";
            this.TheSims3CheatsCloseButton.UseVisualStyleBackColor = true;
            this.TheSims3CheatsCloseButton.Click += new System.EventHandler(this.TheSims3CheatsCloseButton_Click);
            // 
            // MiscCheatsButton
            // 
            this.MiscCheatsButton.Location = new System.Drawing.Point(12, 12);
            this.MiscCheatsButton.Name = "MiscCheatsButton";
            this.MiscCheatsButton.Size = new System.Drawing.Size(118, 23);
            this.MiscCheatsButton.TabIndex = 1;
            this.MiscCheatsButton.Text = "Miscellaneous Cheats";
            this.MiscCheatsButton.UseVisualStyleBackColor = true;
            this.MiscCheatsButton.Click += new System.EventHandler(this.MiscCheats_Click);
            // 
            // StorytellingCheatsButton
            // 
            this.StorytellingCheatsButton.Location = new System.Drawing.Point(12, 41);
            this.StorytellingCheatsButton.Name = "StorytellingCheatsButton";
            this.StorytellingCheatsButton.Size = new System.Drawing.Size(102, 23);
            this.StorytellingCheatsButton.TabIndex = 2;
            this.StorytellingCheatsButton.Text = "Storytelling Cheats";
            this.StorytellingCheatsButton.UseVisualStyleBackColor = true;
            this.StorytellingCheatsButton.Click += new System.EventHandler(this.StorytellingCheats_Click);
            // 
            // BuildCheatsButton
            // 
            this.BuildCheatsButton.Location = new System.Drawing.Point(12, 70);
            this.BuildCheatsButton.Name = "BuildCheatsButton";
            this.BuildCheatsButton.Size = new System.Drawing.Size(74, 23);
            this.BuildCheatsButton.TabIndex = 3;
            this.BuildCheatsButton.Text = "Build Cheats";
            this.BuildCheatsButton.UseVisualStyleBackColor = true;
            this.BuildCheatsButton.Click += new System.EventHandler(this.BuildCheatsButton_Click);
            // 
            // GameplayCheatsButton
            // 
            this.GameplayCheatsButton.Location = new System.Drawing.Point(12, 99);
            this.GameplayCheatsButton.Name = "GameplayCheatsButton";
            this.GameplayCheatsButton.Size = new System.Drawing.Size(98, 23);
            this.GameplayCheatsButton.TabIndex = 4;
            this.GameplayCheatsButton.Text = "Gameplay Cheats";
            this.GameplayCheatsButton.UseVisualStyleBackColor = true;
            this.GameplayCheatsButton.Click += new System.EventHandler(this.GameplayCheatsButton_Click);
            // 
            // AdvancedCheatsButton
            // 
            this.AdvancedCheatsButton.Location = new System.Drawing.Point(12, 128);
            this.AdvancedCheatsButton.Name = "AdvancedCheatsButton";
            this.AdvancedCheatsButton.Size = new System.Drawing.Size(102, 23);
            this.AdvancedCheatsButton.TabIndex = 5;
            this.AdvancedCheatsButton.Text = "Advanced Cheats";
            this.AdvancedCheatsButton.UseVisualStyleBackColor = true;
            this.AdvancedCheatsButton.Click += new System.EventHandler(this.AdvancedCheatsButton_Click);
            // 
            // TestingCheatsButton
            // 
            this.TestingCheatsButton.Location = new System.Drawing.Point(12, 157);
            this.TestingCheatsButton.Name = "TestingCheatsButton";
            this.TestingCheatsButton.Size = new System.Drawing.Size(86, 23);
            this.TestingCheatsButton.TabIndex = 7;
            this.TestingCheatsButton.Text = "Testing Cheats";
            this.TestingCheatsButton.UseVisualStyleBackColor = true;
            this.TestingCheatsButton.Click += new System.EventHandler(this.TestingCheatsButton_Click);
            // 
            // TheSims3Cheats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(224, 215);
            this.Controls.Add(this.TestingCheatsButton);
            this.Controls.Add(this.AdvancedCheatsButton);
            this.Controls.Add(this.GameplayCheatsButton);
            this.Controls.Add(this.BuildCheatsButton);
            this.Controls.Add(this.StorytellingCheatsButton);
            this.Controls.Add(this.MiscCheatsButton);
            this.Controls.Add(this.TheSims3CheatsCloseButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TheSims3Cheats";
            this.Text = "The Sims 3 Cheats";
            this.Load += new System.EventHandler(this.TheSims3Cheats_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button TheSims3CheatsCloseButton;
        private System.Windows.Forms.Button MiscCheatsButton;
        private System.Windows.Forms.Button StorytellingCheatsButton;
        private System.Windows.Forms.Button BuildCheatsButton;
        private System.Windows.Forms.Button GameplayCheatsButton;
        private System.Windows.Forms.Button AdvancedCheatsButton;
        private System.Windows.Forms.Button TestingCheatsButton;
    }
}