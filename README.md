# Cheatcode Reference Program

Simple reference program for cheat-codes for different PC games.

I will update the program when I can with new games that I, my girlfriend, or close friends play. If someone would like to recommend a game for me to add feel free to contact me.

I am new to C# programming. Any help and tips would be greatly appreciated.