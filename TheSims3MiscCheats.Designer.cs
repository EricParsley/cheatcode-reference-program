﻿namespace Cheatcode_Reference_Program
{
    partial class TheSims3MiscCheats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TheSims3MiscCheats));
            this.CloseMiscCheats = new System.Windows.Forms.Button();
            this.MiscCheatRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // CloseMiscCheats
            // 
            this.CloseMiscCheats.Location = new System.Drawing.Point(544, 222);
            this.CloseMiscCheats.Name = "CloseMiscCheats";
            this.CloseMiscCheats.Size = new System.Drawing.Size(75, 23);
            this.CloseMiscCheats.TabIndex = 1;
            this.CloseMiscCheats.Text = "Close";
            this.CloseMiscCheats.UseVisualStyleBackColor = true;
            this.CloseMiscCheats.Click += new System.EventHandler(this.Close_Click);
            // 
            // MiscCheatRichTextBox
            // 
            this.MiscCheatRichTextBox.Location = new System.Drawing.Point(12, 12);
            this.MiscCheatRichTextBox.Name = "MiscCheatRichTextBox";
            this.MiscCheatRichTextBox.ReadOnly = true;
            this.MiscCheatRichTextBox.Size = new System.Drawing.Size(607, 204);
            this.MiscCheatRichTextBox.TabIndex = 2;
            this.MiscCheatRichTextBox.Text = resources.GetString("MiscCheatRichTextBox.Text");
            this.MiscCheatRichTextBox.TextChanged += new System.EventHandler(this.MiscCheatRichTextBox_TextChanged);
            // 
            // TheSims3MiscCheats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 251);
            this.Controls.Add(this.MiscCheatRichTextBox);
            this.Controls.Add(this.CloseMiscCheats);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TheSims3MiscCheats";
            this.Text = "The SIms 3 Miscellaneous Cheats";
            this.Load += new System.EventHandler(this.MiscellaneousCheats_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button CloseMiscCheats;
        private System.Windows.Forms.RichTextBox MiscCheatRichTextBox;
    }
}