﻿namespace Cheatcode_Reference_Program
{
    partial class TheSims3BuildCheats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TheSims3BuildCheats));
            this.TheSims3BuildCheatCloseButton = new System.Windows.Forms.Button();
            this.BuildCheatsRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // TheSims3BuildCheatCloseButton
            // 
            this.TheSims3BuildCheatCloseButton.Location = new System.Drawing.Point(713, 259);
            this.TheSims3BuildCheatCloseButton.Name = "TheSims3BuildCheatCloseButton";
            this.TheSims3BuildCheatCloseButton.Size = new System.Drawing.Size(75, 23);
            this.TheSims3BuildCheatCloseButton.TabIndex = 0;
            this.TheSims3BuildCheatCloseButton.Text = "Close";
            this.TheSims3BuildCheatCloseButton.UseVisualStyleBackColor = true;
            this.TheSims3BuildCheatCloseButton.Click += new System.EventHandler(this.TheSims3BuildCheatCloseButton_Click);
            // 
            // BuildCheatsRichTextBox
            // 
            this.BuildCheatsRichTextBox.Location = new System.Drawing.Point(12, 12);
            this.BuildCheatsRichTextBox.Name = "BuildCheatsRichTextBox";
            this.BuildCheatsRichTextBox.Size = new System.Drawing.Size(776, 241);
            this.BuildCheatsRichTextBox.TabIndex = 1;
            this.BuildCheatsRichTextBox.Text = resources.GetString("BuildCheatsRichTextBox.Text");
            // 
            // TheSims3BuildCheats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 291);
            this.Controls.Add(this.BuildCheatsRichTextBox);
            this.Controls.Add(this.TheSims3BuildCheatCloseButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TheSims3BuildCheats";
            this.Text = "The Sims 3 Build Cheats";
            this.Load += new System.EventHandler(this.TheSims3BuildCheats_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button TheSims3BuildCheatCloseButton;
        private System.Windows.Forms.RichTextBox BuildCheatsRichTextBox;
    }
}