﻿namespace Cheatcode_Reference_Program
{
    partial class TheSims3AdvancedCheats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TheSims3AdvancedCheats));
            this.TheSims3AdvancedCheatCloseButton = new System.Windows.Forms.Button();
            this.AdvancedCheatsRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // TheSims3AdvancedCheatCloseButton
            // 
            this.TheSims3AdvancedCheatCloseButton.Location = new System.Drawing.Point(713, 377);
            this.TheSims3AdvancedCheatCloseButton.Name = "TheSims3AdvancedCheatCloseButton";
            this.TheSims3AdvancedCheatCloseButton.Size = new System.Drawing.Size(75, 23);
            this.TheSims3AdvancedCheatCloseButton.TabIndex = 0;
            this.TheSims3AdvancedCheatCloseButton.Text = "Close";
            this.TheSims3AdvancedCheatCloseButton.UseVisualStyleBackColor = true;
            this.TheSims3AdvancedCheatCloseButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // AdvancedCheatsRichTextBox
            // 
            this.AdvancedCheatsRichTextBox.Location = new System.Drawing.Point(12, 12);
            this.AdvancedCheatsRichTextBox.Name = "AdvancedCheatsRichTextBox";
            this.AdvancedCheatsRichTextBox.Size = new System.Drawing.Size(776, 359);
            this.AdvancedCheatsRichTextBox.TabIndex = 1;
            this.AdvancedCheatsRichTextBox.Text = resources.GetString("AdvancedCheatsRichTextBox.Text");
            // 
            // TheSims3AdvancedCheats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 407);
            this.Controls.Add(this.AdvancedCheatsRichTextBox);
            this.Controls.Add(this.TheSims3AdvancedCheatCloseButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TheSims3AdvancedCheats";
            this.Text = "The Sims 3 Advanced Cheats";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button TheSims3AdvancedCheatCloseButton;
        private System.Windows.Forms.RichTextBox AdvancedCheatsRichTextBox;
    }
}